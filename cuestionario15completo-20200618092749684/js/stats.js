var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "24283",
        "ok": "24280",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "210",
        "ko": "18"
    },
    "maxResponseTime": {
        "total": "4497",
        "ok": "4497",
        "ko": "523"
    },
    "meanResponseTime": {
        "total": "1021",
        "ok": "1022",
        "ko": "237"
    },
    "standardDeviation": {
        "total": "503",
        "ok": "503",
        "ko": "212"
    },
    "percentiles1": {
        "total": "1291",
        "ok": "1291",
        "ko": "170"
    },
    "percentiles2": {
        "total": "1306",
        "ok": "1306",
        "ko": "347"
    },
    "percentiles3": {
        "total": "1541",
        "ok": "1540",
        "ko": "488"
    },
    "percentiles4": {
        "total": "2222",
        "ok": "2222",
        "ko": "516"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6806,
    "percentage": 28
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3902,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 13572,
    "percentage": 56
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "33.54",
        "ok": "33.536",
        "ko": "0.004"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "844",
        "ok": "844",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "955",
        "ok": "955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "269",
        "ok": "269",
        "ko": "-"
    },
    "percentiles1": {
        "total": "861",
        "ok": "861",
        "ko": "-"
    },
    "percentiles2": {
        "total": "868",
        "ok": "868",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 791,
    "percentage": 88
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 109,
    "percentage": 12
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1881",
        "ok": "1881",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "101",
        "ok": "101",
        "ko": "-"
    },
    "percentiles1": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles2": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles3": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "738",
        "ok": "738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 899,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "755",
        "ok": "755",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "percentiles1": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles3": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles4": {
        "total": "730",
        "ok": "730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 900,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1431",
        "ok": "1431",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "306",
        "ok": "306",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "238",
        "ok": "239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "657",
        "ok": "657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 867,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 30,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "309",
        "ok": "309",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2971",
        "ok": "2971",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "955",
        "ok": "955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "338",
        "ok": "338",
        "ko": "-"
    },
    "percentiles1": {
        "total": "968",
        "ok": "968",
        "ko": "-"
    },
    "percentiles2": {
        "total": "991",
        "ok": "991",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1525",
        "ok": "1525",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2185",
        "ok": "2185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 132,
    "percentage": 15
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 680,
    "percentage": 76
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 88,
    "percentage": 10
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "305",
        "ok": "305",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles1": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "percentiles3": {
        "total": "683",
        "ok": "683",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1170",
        "ok": "1170",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 861,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 30,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 9,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "900",
        "ok": "899",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "516",
        "ok": "516",
        "ko": "523"
    },
    "maxResponseTime": {
        "total": "4198",
        "ok": "4198",
        "ko": "523"
    },
    "meanResponseTime": {
        "total": "1164",
        "ok": "1164",
        "ko": "523"
    },
    "standardDeviation": {
        "total": "356",
        "ok": "355",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1176",
        "ok": "1176",
        "ko": "523"
    },
    "percentiles2": {
        "total": "1187",
        "ok": "1187",
        "ko": "523"
    },
    "percentiles3": {
        "total": "1700",
        "ok": "1700",
        "ko": "523"
    },
    "percentiles4": {
        "total": "2557",
        "ok": "2558",
        "ko": "523"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 119,
    "percentage": 13
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 635,
    "percentage": 71
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 145,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.242",
        "ko": "0.001"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "874",
        "ok": "874",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "115",
        "ok": "115",
        "ko": "-"
    },
    "percentiles1": {
        "total": "860",
        "ok": "860",
        "ko": "-"
    },
    "percentiles2": {
        "total": "865",
        "ok": "865",
        "ko": "-"
    },
    "percentiles3": {
        "total": "880",
        "ok": "880",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1384",
        "ok": "1384",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 884,
    "percentage": 98
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 15,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "903",
        "ok": "903",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "324",
        "ok": "324",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles1": {
        "total": "309",
        "ok": "309",
        "ko": "-"
    },
    "percentiles2": {
        "total": "318",
        "ok": "318",
        "ko": "-"
    },
    "percentiles3": {
        "total": "377",
        "ok": "377",
        "ko": "-"
    },
    "percentiles4": {
        "total": "829",
        "ok": "829",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 887,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "900",
        "ok": "900",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1283",
        "ok": "1283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "82",
        "ok": "82",
        "ko": "-"
    },
    "percentiles1": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles3": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "644",
        "ok": "644",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 894,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.243",
        "ok": "1.243",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-d2054": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--abrir-s-d2054",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "314",
        "ok": "314",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2283",
        "ok": "2283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "296",
        "ok": "296",
        "ko": "-"
    },
    "percentiles1": {
        "total": "972",
        "ok": "972",
        "ko": "-"
    },
    "percentiles2": {
        "total": "999",
        "ok": "999",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1496",
        "ok": "1496",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2170",
        "ok": "2170",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 98,
    "percentage": 11
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 722,
    "percentage": 80
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 79,
    "percentage": 9
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-1b83b": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
pathFormatted: "req_siette--abrir-s-1b83b",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "641",
        "ok": "641",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "723",
        "ok": "723",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "96",
        "ok": "96",
        "ko": "-"
    },
    "percentiles1": {
        "total": "696",
        "ok": "695",
        "ko": "-"
    },
    "percentiles2": {
        "total": "739",
        "ok": "739",
        "ko": "-"
    },
    "percentiles3": {
        "total": "886",
        "ok": "886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1204",
        "ok": "1204",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 827,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 62,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 10,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_siette--confirm-0c992": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--confirm-0c992",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3676",
        "ok": "3676",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "370",
        "ok": "370",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1312",
        "ok": "1312",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2741",
        "ok": "2741",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 65,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 826,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1363",
        "ok": "1363",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "291",
        "ok": "291",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1313",
        "ok": "1313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1963",
        "ok": "1963",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 875,
    "percentage": 97
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1370",
        "ok": "1370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1305",
        "ok": "1304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1313",
        "ok": "1313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2587",
        "ok": "2587",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 10,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 883,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "899",
        "ok": "897",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "18",
        "ok": "441",
        "ko": "18"
    },
    "maxResponseTime": {
        "total": "4211",
        "ok": "4211",
        "ko": "170"
    },
    "meanResponseTime": {
        "total": "1379",
        "ok": "1382",
        "ko": "94"
    },
    "standardDeviation": {
        "total": "307",
        "ok": "302",
        "ko": "76"
    },
    "percentiles1": {
        "total": "1305",
        "ok": "1305",
        "ko": "94"
    },
    "percentiles2": {
        "total": "1314",
        "ok": "1314",
        "ko": "132"
    },
    "percentiles3": {
        "total": "2000",
        "ok": "2001",
        "ko": "162"
    },
    "percentiles4": {
        "total": "2760",
        "ok": "2763",
        "ko": "168"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 1
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 884,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.239",
        "ko": "0.003"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3714",
        "ok": "3714",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1131",
        "ok": "1131",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "444",
        "ok": "444",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1300",
        "ok": "1300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1310",
        "ok": "1310",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1525",
        "ok": "1525",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 220,
    "percentage": 24
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 656,
    "percentage": 73
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4497",
        "ok": "4497",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1381",
        "ok": "1381",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "287",
        "ok": "287",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1304",
        "ok": "1304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1313",
        "ok": "1313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 897,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3111",
        "ok": "3111",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1346",
        "ok": "1346",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1311",
        "ok": "1311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1560",
        "ok": "1560",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3817",
        "ok": "3817",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1369",
        "ok": "1369",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1311",
        "ok": "1311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1360",
        "ok": "1360",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1304",
        "ok": "1304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1313",
        "ok": "1313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1725",
        "ok": "1725",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1283",
        "ok": "1283",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3395",
        "ok": "3395",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1350",
        "ok": "1350",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1303",
        "ok": "1303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1311",
        "ok": "1311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2215",
        "ok": "2215",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3713",
        "ok": "3713",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1346",
        "ok": "1346",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1301",
        "ok": "1301",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1308",
        "ok": "1308",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1524",
        "ok": "1524",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2654",
        "ok": "2654",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3160",
        "ok": "3160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1336",
        "ok": "1336",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "168",
        "ok": "168",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1300",
        "ok": "1300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1307",
        "ok": "1307",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2050",
        "ok": "2050",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1278",
        "ok": "1278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3873",
        "ok": "3873",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1331",
        "ok": "1331",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1300",
        "ok": "1300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1307",
        "ok": "1307",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1391",
        "ok": "1391",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2173",
        "ok": "2173",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1279",
        "ok": "1279",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "199",
        "ok": "199",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1300",
        "ok": "1300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1307",
        "ok": "1307",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2279",
        "ok": "2279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "899",
        "ok": "899",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1291",
        "ok": "1291",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3583",
        "ok": "3583",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1569",
        "ok": "1569",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1525",
        "ok": "1525",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1534",
        "ok": "1534",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1766",
        "ok": "1766",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 899,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.242",
        "ok": "1.242",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
