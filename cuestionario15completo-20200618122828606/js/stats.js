var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "62997",
        "ok": "62996",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "130518"
    },
    "maxResponseTime": {
        "total": "130518",
        "ok": "2073",
        "ko": "130518"
    },
    "meanResponseTime": {
        "total": "307",
        "ok": "305",
        "ko": "130518"
    },
    "standardDeviation": {
        "total": "557",
        "ok": "204",
        "ko": "0"
    },
    "percentiles1": {
        "total": "217",
        "ok": "217",
        "ko": "130518"
    },
    "percentiles2": {
        "total": "248",
        "ok": "248",
        "ko": "130518"
    },
    "percentiles3": {
        "total": "853",
        "ok": "853",
        "ko": "130518"
    },
    "percentiles4": {
        "total": "859",
        "ok": "859",
        "ko": "130518"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 55874,
    "percentage": 89
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7093,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 29,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "63.698",
        "ok": "63.697",
        "ko": "0.001"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "7000",
        "ok": "6999",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "838",
        "ok": "838",
        "ko": "130518"
    },
    "maxResponseTime": {
        "total": "130518",
        "ok": "2073",
        "ko": "130518"
    },
    "meanResponseTime": {
        "total": "875",
        "ok": "856",
        "ko": "130518"
    },
    "standardDeviation": {
        "total": "1551",
        "ok": "58",
        "ko": "0"
    },
    "percentiles1": {
        "total": "852",
        "ok": "852",
        "ko": "130518"
    },
    "percentiles2": {
        "total": "856",
        "ok": "856",
        "ko": "130518"
    },
    "percentiles3": {
        "total": "860",
        "ok": "860",
        "ko": "130518"
    },
    "percentiles4": {
        "total": "916",
        "ok": "913",
        "ko": "130518"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6971,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 28,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.077",
        "ko": "0.001"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "6999",
        "ok": "6999",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "725",
        "ok": "725",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.077",
        "ok": "7.077",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "6999",
        "ok": "6999",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "455",
        "ok": "455",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.077",
        "ok": "7.077",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "6999",
        "ok": "6999",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "861",
        "ok": "861",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "percentiles1": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles4": {
        "total": "435",
        "ok": "435",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.077",
        "ok": "7.077",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "296",
        "ok": "296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1034",
        "ok": "1034",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "328",
        "ok": "328",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "percentiles1": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "percentiles2": {
        "total": "319",
        "ok": "319",
        "ko": "-"
    },
    "percentiles3": {
        "total": "362",
        "ok": "362",
        "ko": "-"
    },
    "percentiles4": {
        "total": "948",
        "ok": "948",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6881,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 119,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.078",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1790",
        "ok": "1790",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "percentiles1": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles2": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles4": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.078",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "850",
        "ok": "850",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.078",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.078",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "847",
        "ok": "847",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles2": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles3": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "426",
        "ok": "426",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.078",
        "ok": "7.078",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
