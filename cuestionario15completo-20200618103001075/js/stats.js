var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "28000",
        "ok": "28000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1762",
        "ok": "1762",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "385",
        "ok": "385",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles1": {
        "total": "435",
        "ok": "435",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "684",
        "ok": "684",
        "ko": "-"
    },
    "percentiles4": {
        "total": "859",
        "ok": "859",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 26970,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1025,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "90.032",
        "ok": "90.032",
        "ko": "-"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "843",
        "ok": "843",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1364",
        "ok": "1364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "857",
        "ok": "857",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles1": {
        "total": "855",
        "ok": "855",
        "ko": "-"
    },
    "percentiles2": {
        "total": "859",
        "ok": "859",
        "ko": "-"
    },
    "percentiles3": {
        "total": "865",
        "ok": "865",
        "ko": "-"
    },
    "percentiles4": {
        "total": "889",
        "ok": "889",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 999,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6",
        "ok": "6",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "722",
        "ok": "722",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles4": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "percentiles1": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "301",
        "ok": "301",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles1": {
        "total": "315",
        "ok": "315",
        "ko": "-"
    },
    "percentiles2": {
        "total": "325",
        "ok": "325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "365",
        "ok": "365",
        "ko": "-"
    },
    "percentiles4": {
        "total": "399",
        "ok": "399",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "887",
        "ok": "887",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles4": {
        "total": "279",
        "ok": "279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "516",
        "ok": "516",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "966",
        "ok": "966",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "539",
        "ok": "539",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "538",
        "ok": "538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "543",
        "ok": "543",
        "ko": "-"
    },
    "percentiles3": {
        "total": "553",
        "ok": "553",
        "ko": "-"
    },
    "percentiles4": {
        "total": "571",
        "ok": "571",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bae30": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA",
pathFormatted: "req_siette--abrir-s-bae30",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "304",
        "ok": "304",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "455",
        "ok": "455",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "324",
        "ok": "324",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles1": {
        "total": "317",
        "ok": "317",
        "ko": "-"
    },
    "percentiles2": {
        "total": "327",
        "ok": "327",
        "ko": "-"
    },
    "percentiles3": {
        "total": "369",
        "ok": "369",
        "ko": "-"
    },
    "percentiles4": {
        "total": "394",
        "ok": "394",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-03cba": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA Redirect 1",
pathFormatted: "req_siette--abrir-s-03cba",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA Redirect 1",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "418",
        "ok": "418",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "895",
        "ok": "895",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "665",
        "ok": "665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "percentiles1": {
        "total": "672",
        "ok": "672",
        "ko": "-"
    },
    "percentiles2": {
        "total": "696",
        "ok": "696",
        "ko": "-"
    },
    "percentiles3": {
        "total": "736",
        "ok": "736",
        "ko": "-"
    },
    "percentiles4": {
        "total": "785",
        "ok": "785",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 992,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--confirm-4bdd4": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA",
pathFormatted: "req_siette--confirm-4bdd4",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "465",
        "ok": "465",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "444",
        "ok": "444",
        "ko": "-"
    },
    "percentiles3": {
        "total": "649",
        "ok": "649",
        "ko": "-"
    },
    "percentiles4": {
        "total": "656",
        "ok": "656",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1336",
        "ok": "1336",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "444",
        "ok": "444",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "448",
        "ok": "448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "646",
        "ok": "646",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "701",
        "ok": "701",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "percentiles4": {
        "total": "646",
        "ok": "646",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "706",
        "ok": "706",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "447",
        "ok": "447",
        "ko": "-"
    },
    "percentiles4": {
        "total": "514",
        "ok": "514",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "873",
        "ok": "873",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "448",
        "ok": "448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1066",
        "ok": "1066",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "percentiles4": {
        "total": "504",
        "ok": "504",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "897",
        "ok": "897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles4": {
        "total": "483",
        "ok": "483",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "869",
        "ok": "869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles4": {
        "total": "488",
        "ok": "488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1283",
        "ok": "1283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "443",
        "ok": "443",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles1": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "percentiles2": {
        "total": "443",
        "ok": "443",
        "ko": "-"
    },
    "percentiles3": {
        "total": "451",
        "ok": "451",
        "ko": "-"
    },
    "percentiles4": {
        "total": "491",
        "ok": "491",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "900",
        "ok": "900",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles4": {
        "total": "477",
        "ok": "477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "492",
        "ok": "492",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles1": {
        "total": "437",
        "ok": "437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles3": {
        "total": "444",
        "ok": "444",
        "ko": "-"
    },
    "percentiles4": {
        "total": "479",
        "ok": "479",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "854",
        "ok": "854",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "367",
        "ok": "367",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "percentiles1": {
        "total": "435",
        "ok": "435",
        "ko": "-"
    },
    "percentiles2": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles3": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles4": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1762",
        "ok": "1762",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "108",
        "ok": "108",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles3": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "percentiles4": {
        "total": "443",
        "ok": "443",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "650",
        "ok": "650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "percentiles4": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "892",
        "ok": "892",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles1": {
        "total": "448",
        "ok": "448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "451",
        "ok": "451",
        "ko": "-"
    },
    "percentiles3": {
        "total": "487",
        "ok": "487",
        "ko": "-"
    },
    "percentiles4": {
        "total": "660",
        "ok": "660",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "652",
        "ok": "652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "234",
        "ok": "234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles2": {
        "total": "233",
        "ok": "234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "279",
        "ok": "279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "669",
        "ok": "669",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "295",
        "ok": "295",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "796",
        "ok": "796",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "319",
        "ok": "319",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "percentiles3": {
        "total": "368",
        "ok": "368",
        "ko": "-"
    },
    "percentiles4": {
        "total": "392",
        "ok": "392",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles2": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles3": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.215",
        "ok": "3.215",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
