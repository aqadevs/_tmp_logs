var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "195994",
        "ok": "195992",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "129162"
    },
    "maxResponseTime": {
        "total": "130542",
        "ok": "2455",
        "ko": "130542"
    },
    "meanResponseTime": {
        "total": "350",
        "ok": "349",
        "ko": "129852"
    },
    "standardDeviation": {
        "total": "458",
        "ok": "197",
        "ko": "690"
    },
    "percentiles1": {
        "total": "231",
        "ok": "231",
        "ko": "129852"
    },
    "percentiles2": {
        "total": "436",
        "ok": "436",
        "ko": "130197"
    },
    "percentiles3": {
        "total": "746",
        "ok": "746",
        "ko": "130473"
    },
    "percentiles4": {
        "total": "861",
        "ok": "861",
        "ko": "130528"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 187386,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8212,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 394,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "159.604",
        "ok": "159.603",
        "ko": "0.002"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "7000",
        "ok": "6998",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "838",
        "ok": "838",
        "ko": "129162"
    },
    "maxResponseTime": {
        "total": "130542",
        "ok": "2071",
        "ko": "130542"
    },
    "meanResponseTime": {
        "total": "893",
        "ok": "856",
        "ko": "129852"
    },
    "standardDeviation": {
        "total": "2180",
        "ok": "38",
        "ko": "690"
    },
    "percentiles1": {
        "total": "853",
        "ok": "853",
        "ko": "129852"
    },
    "percentiles2": {
        "total": "857",
        "ok": "857",
        "ko": "130197"
    },
    "percentiles3": {
        "total": "869",
        "ok": "869",
        "ko": "130473"
    },
    "percentiles4": {
        "total": "911",
        "ok": "910",
        "ko": "130528"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6984,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 14,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.699",
        "ko": "0.002"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "6998",
        "ok": "6998",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "751",
        "ok": "751",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles4": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.699",
        "ok": "5.699",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "6998",
        "ok": "6998",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "759",
        "ok": "759",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles4": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.699",
        "ok": "5.699",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "6998",
        "ok": "6998",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1137",
        "ok": "1137",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles2": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles3": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "percentiles4": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6989,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.699",
        "ok": "5.699",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "300",
        "ok": "300",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1476",
        "ok": "1476",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "361",
        "ok": "361",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "106",
        "ok": "106",
        "ko": "-"
    },
    "percentiles1": {
        "total": "328",
        "ok": "328",
        "ko": "-"
    },
    "percentiles2": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "503",
        "ok": "503",
        "ko": "-"
    },
    "percentiles4": {
        "total": "962",
        "ok": "962",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6851,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 146,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1219",
        "ok": "1219",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "231",
        "ok": "231",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "260",
        "ok": "260",
        "ko": "-"
    },
    "percentiles4": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6994,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1815",
        "ok": "1815",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "524",
        "ok": "524",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "percentiles1": {
        "total": "510",
        "ok": "510",
        "ko": "-"
    },
    "percentiles2": {
        "total": "520",
        "ok": "520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "574",
        "ok": "574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "786",
        "ok": "786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6933,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 53,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 14,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-7631f": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D",
pathFormatted: "req_siette--abrir-s-7631f",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1093",
        "ok": "1093",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "351",
        "ok": "351",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "67",
        "ok": "67",
        "ko": "-"
    },
    "percentiles1": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "percentiles2": {
        "total": "359",
        "ok": "360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "460",
        "ok": "460",
        "ko": "-"
    },
    "percentiles4": {
        "total": "661",
        "ok": "661",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6974,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-11dae": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D Redirect 1",
pathFormatted: "req_siette--abrir-s-11dae",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - D Redirect 1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "643",
        "ok": "643",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2455",
        "ok": "2455",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "755",
        "ok": "755",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles1": {
        "total": "714",
        "ok": "714",
        "ko": "-"
    },
    "percentiles2": {
        "total": "755",
        "ok": "755",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1025",
        "ok": "1024",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6038,
    "percentage": 86
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 778,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 184,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--confirm-2c23d": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - D",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - D",
pathFormatted: "req_siette--confirm-2c23d",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - D",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "660",
        "ok": "660",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "percentiles1": {
        "total": "648",
        "ok": "648",
        "ko": "-"
    },
    "percentiles2": {
        "total": "652",
        "ok": "652",
        "ko": "-"
    },
    "percentiles3": {
        "total": "691",
        "ok": "691",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1288",
        "ok": "1288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6843,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 32,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 125,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1783",
        "ok": "1783",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "651",
        "ok": "651",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "652",
        "ok": "652",
        "ko": "-"
    },
    "percentiles2": {
        "total": "655",
        "ok": "655",
        "ko": "-"
    },
    "percentiles3": {
        "total": "687",
        "ok": "687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "730",
        "ok": "730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6971,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1530",
        "ok": "1530",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "637",
        "ok": "637",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "percentiles1": {
        "total": "651",
        "ok": "651",
        "ko": "-"
    },
    "percentiles2": {
        "total": "655",
        "ok": "655",
        "ko": "-"
    },
    "percentiles3": {
        "total": "685",
        "ok": "685",
        "ko": "-"
    },
    "percentiles4": {
        "total": "737",
        "ok": "737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6957,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 36,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 7,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1977",
        "ok": "1977",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "76",
        "ok": "76",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "647",
        "ok": "647",
        "ko": "-"
    },
    "percentiles4": {
        "total": "680",
        "ok": "680",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6965,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 15,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1081",
        "ok": "1081",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "378",
        "ok": "378",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6996,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1617",
        "ok": "1617",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6992,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1498",
        "ok": "1498",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "234",
        "ok": "234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "percentiles4": {
        "total": "367",
        "ok": "367",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6993,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1519",
        "ok": "1519",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "231",
        "ok": "231",
        "ko": "-"
    },
    "percentiles3": {
        "total": "269",
        "ok": "269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6993,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles3": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "percentiles4": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6989,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1080",
        "ok": "1080",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "231",
        "ok": "231",
        "ko": "-"
    },
    "percentiles3": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "437",
        "ok": "437",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6996,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1364",
        "ok": "1364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6996,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1990",
        "ok": "1990",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "percentiles4": {
        "total": "435",
        "ok": "435",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6991,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1703",
        "ok": "1703",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "percentiles4": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6992,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1295",
        "ok": "1295",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "percentiles4": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6995,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1979",
        "ok": "1979",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles1": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "percentiles2": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles3": {
        "total": "414",
        "ok": "414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "545",
        "ok": "545",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6985,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 10,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1524",
        "ok": "1524",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles1": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles2": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles3": {
        "total": "274",
        "ok": "273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "343",
        "ok": "343",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6993,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "866",
        "ok": "866",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles4": {
        "total": "253",
        "ok": "253",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "296",
        "ok": "296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1069",
        "ok": "1069",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "348",
        "ok": "348",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "percentiles1": {
        "total": "325",
        "ok": "325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "358",
        "ok": "358",
        "ko": "-"
    },
    "percentiles3": {
        "total": "457",
        "ok": "457",
        "ko": "-"
    },
    "percentiles4": {
        "total": "672",
        "ok": "672",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6971,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 29,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "7000",
        "ok": "7000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1304",
        "ok": "1304",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles3": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles4": {
        "total": "276",
        "ok": "276",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6995,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.7",
        "ok": "5.7",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
