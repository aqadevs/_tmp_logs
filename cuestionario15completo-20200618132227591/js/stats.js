var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "223991",
        "ok": "223988",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "129928"
    },
    "maxResponseTime": {
        "total": "131189",
        "ok": "2067",
        "ko": "131189"
    },
    "meanResponseTime": {
        "total": "345",
        "ok": "343",
        "ko": "130465"
    },
    "standardDeviation": {
        "total": "513",
        "ok": "190",
        "ko": "532"
    },
    "percentiles1": {
        "total": "230",
        "ok": "230",
        "ko": "130277"
    },
    "percentiles2": {
        "total": "432",
        "ok": "432",
        "ko": "130733"
    },
    "percentiles3": {
        "total": "733",
        "ok": "733",
        "ko": "131098"
    },
    "percentiles4": {
        "total": "856",
        "ok": "856",
        "ko": "131171"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 215166,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8630,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 192,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "137.756",
        "ok": "137.754",
        "ko": "0.002"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "8000",
        "ok": "7997",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "838",
        "ok": "838",
        "ko": "129928"
    },
    "maxResponseTime": {
        "total": "131189",
        "ok": "2067",
        "ko": "131189"
    },
    "meanResponseTime": {
        "total": "902",
        "ok": "853",
        "ko": "130465"
    },
    "standardDeviation": {
        "total": "2510",
        "ok": "36",
        "ko": "532"
    },
    "percentiles1": {
        "total": "851",
        "ok": "851",
        "ko": "130277"
    },
    "percentiles2": {
        "total": "855",
        "ok": "855",
        "ko": "130733"
    },
    "percentiles3": {
        "total": "861",
        "ok": "861",
        "ko": "131098"
    },
    "percentiles4": {
        "total": "890",
        "ok": "888",
        "ko": "131171"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7981,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 16,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.918",
        "ko": "0.002"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "7997",
        "ok": "7997",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "735",
        "ok": "735",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.918",
        "ok": "4.918",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "7997",
        "ok": "7997",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "746",
        "ok": "746",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.918",
        "ok": "4.918",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "7997",
        "ok": "7997",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles1": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles2": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "236",
        "ok": "235",
        "ko": "-"
    },
    "percentiles4": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7992,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.918",
        "ok": "4.918",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "298",
        "ok": "298",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1155",
        "ok": "1155",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "345",
        "ok": "345",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles1": {
        "total": "326",
        "ok": "326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "349",
        "ok": "349",
        "ko": "-"
    },
    "percentiles3": {
        "total": "397",
        "ok": "397",
        "ko": "-"
    },
    "percentiles4": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7864,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 136,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1076",
        "ok": "1076",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles1": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "226",
        "ok": "226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "234",
        "ok": "234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "499",
        "ok": "499",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1211",
        "ok": "1211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "546",
        "ok": "546",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "541",
        "ok": "541",
        "ko": "-"
    },
    "percentiles2": {
        "total": "551",
        "ok": "551",
        "ko": "-"
    },
    "percentiles3": {
        "total": "579",
        "ok": "579",
        "ko": "-"
    },
    "percentiles4": {
        "total": "609",
        "ok": "609",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7991,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-02611": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E",
pathFormatted: "req_siette--abrir-s-02611",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "300",
        "ok": "300",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "834",
        "ok": "834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "333",
        "ok": "333",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "325",
        "ok": "325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "344",
        "ok": "344",
        "ko": "-"
    },
    "percentiles3": {
        "total": "384",
        "ok": "384",
        "ko": "-"
    },
    "percentiles4": {
        "total": "413",
        "ok": "413",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-1f61a": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E Redirect 1",
pathFormatted: "req_siette--abrir-s-1f61a",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - E Redirect 1",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "637",
        "ok": "637",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1333",
        "ok": "1333",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "724",
        "ok": "724",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "716",
        "ok": "716",
        "ko": "-"
    },
    "percentiles2": {
        "total": "748",
        "ok": "748",
        "ko": "-"
    },
    "percentiles3": {
        "total": "801",
        "ok": "801",
        "ko": "-"
    },
    "percentiles4": {
        "total": "845",
        "ok": "845",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7574,
    "percentage": 95
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 423,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--confirm-5938e": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - E",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - E",
pathFormatted: "req_siette--confirm-5938e",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - E",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "656",
        "ok": "656",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "percentiles1": {
        "total": "647",
        "ok": "647",
        "ko": "-"
    },
    "percentiles2": {
        "total": "650",
        "ok": "650",
        "ko": "-"
    },
    "percentiles3": {
        "total": "670",
        "ok": "670",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1286",
        "ok": "1286",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7843,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 145,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1491",
        "ok": "1491",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "647",
        "ok": "647",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "651",
        "ok": "651",
        "ko": "-"
    },
    "percentiles2": {
        "total": "654",
        "ok": "654",
        "ko": "-"
    },
    "percentiles3": {
        "total": "663",
        "ok": "663",
        "ko": "-"
    },
    "percentiles4": {
        "total": "699",
        "ok": "699",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7983,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 16,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "631",
        "ok": "631",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "67",
        "ok": "67",
        "ko": "-"
    },
    "percentiles1": {
        "total": "650",
        "ok": "650",
        "ko": "-"
    },
    "percentiles2": {
        "total": "654",
        "ok": "654",
        "ko": "-"
    },
    "percentiles3": {
        "total": "663",
        "ok": "663",
        "ko": "-"
    },
    "percentiles4": {
        "total": "700",
        "ok": "700",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7985,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 14,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1487",
        "ok": "1487",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "451",
        "ok": "451",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles1": {
        "total": "437",
        "ok": "437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "percentiles3": {
        "total": "522",
        "ok": "522",
        "ko": "-"
    },
    "percentiles4": {
        "total": "655",
        "ok": "655",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7972,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 10,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "882",
        "ok": "882",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "276",
        "ok": "276",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7997,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1073",
        "ok": "1073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "18",
        "ok": "18",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "276",
        "ok": "276",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "278",
        "ok": "278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "240",
        "ok": "240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1319",
        "ok": "1319",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles1": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles3": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "percentiles4": {
        "total": "288",
        "ok": "288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1111",
        "ok": "1111",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles1": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "230",
        "ok": "230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "240",
        "ok": "240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "278",
        "ok": "278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "879",
        "ok": "879",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7996,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1473",
        "ok": "1473",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7996,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "868",
        "ok": "868",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "18",
        "ok": "18",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "277",
        "ok": "277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "866",
        "ok": "866",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1137",
        "ok": "1137",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "242",
        "ok": "242",
        "ko": "-"
    },
    "percentiles3": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "309",
        "ok": "309",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "862",
        "ok": "862",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "percentiles2": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles3": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "289",
        "ok": "289",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7999,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "628",
        "ok": "628",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "295",
        "ok": "295",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "781",
        "ok": "781",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "330",
        "ok": "330",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles1": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "percentiles2": {
        "total": "344",
        "ok": "344",
        "ko": "-"
    },
    "percentiles3": {
        "total": "386",
        "ok": "386",
        "ko": "-"
    },
    "percentiles4": {
        "total": "411",
        "ok": "411",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1107",
        "ok": "1107",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles2": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "percentiles3": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7998,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.92",
        "ok": "4.92",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
