var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "28",
        "ok": "28",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "392",
        "ok": "392",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles1": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "743",
        "ok": "743",
        "ko": "-"
    },
    "percentiles4": {
        "total": "985",
        "ok": "985",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 26,
    "percentage": 93
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.333",
        "ok": "2.333",
        "ko": "-"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1030",
        "ok": "1030",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles2": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles3": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles4": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "percentiles2": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "percentiles3": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "percentiles4": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "percentiles2": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "percentiles3": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "percentiles4": {
        "total": "518",
        "ok": "518",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-d2054": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--abrir-s-d2054",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "percentiles2": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "percentiles3": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "percentiles4": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-1b83b": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
pathFormatted: "req_siette--abrir-s-1b83b",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "percentiles2": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "percentiles3": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "percentiles4": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--confirm-0c992": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--confirm-0c992",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles2": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles3": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles4": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles2": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles3": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles4": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles2": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles3": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "percentiles4": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "percentiles2": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "percentiles3": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "percentiles4": {
        "total": "436",
        "ok": "436",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles2": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles3": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles4": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles2": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles3": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles4": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles2": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles3": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles4": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles2": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles3": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "percentiles4": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles2": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles3": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles4": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles4": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "percentiles2": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "percentiles3": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles2": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles3": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles4": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.083",
        "ok": "0.083",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
