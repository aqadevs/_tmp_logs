var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "168000",
        "ok": "168000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7722",
        "ok": "7722",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "419",
        "ok": "419",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "percentiles1": {
        "total": "437",
        "ok": "437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "452",
        "ok": "452",
        "ko": "-"
    },
    "percentiles3": {
        "total": "847",
        "ok": "847",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1113",
        "ok": "1113",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 158677,
    "percentage": 94
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7894,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1429,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "274.959",
        "ok": "274.959",
        "ko": "-"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "841",
        "ok": "841",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2865",
        "ok": "2865",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "860",
        "ok": "860",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles1": {
        "total": "855",
        "ok": "855",
        "ko": "-"
    },
    "percentiles2": {
        "total": "859",
        "ok": "859",
        "ko": "-"
    },
    "percentiles3": {
        "total": "870",
        "ok": "870",
        "ko": "-"
    },
    "percentiles4": {
        "total": "917",
        "ok": "917",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5972,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 28,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "746",
        "ok": "746",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "232",
        "ok": "232",
        "ko": "-"
    },
    "percentiles4": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "733",
        "ok": "733",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles1": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "260",
        "ok": "260",
        "ko": "-"
    },
    "percentiles4": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5986,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "299",
        "ok": "299",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "971",
        "ok": "971",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "361",
        "ok": "361",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "67",
        "ok": "67",
        "ko": "-"
    },
    "percentiles1": {
        "total": "337",
        "ok": "337",
        "ko": "-"
    },
    "percentiles2": {
        "total": "380",
        "ok": "380",
        "ko": "-"
    },
    "percentiles3": {
        "total": "499",
        "ok": "499",
        "ko": "-"
    },
    "percentiles4": {
        "total": "613",
        "ok": "613",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5991,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2167",
        "ok": "2167",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "240",
        "ok": "240",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "percentiles3": {
        "total": "287",
        "ok": "287",
        "ko": "-"
    },
    "percentiles4": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5980,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 13,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "342",
        "ok": "342",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7722",
        "ok": "7722",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "799",
        "ok": "799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "702",
        "ok": "702",
        "ko": "-"
    },
    "percentiles1": {
        "total": "549",
        "ok": "549",
        "ko": "-"
    },
    "percentiles2": {
        "total": "600",
        "ok": "600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3577",
        "ok": "3577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5072,
    "percentage": 85
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 202,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 726,
    "percentage": 12
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-d2054": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--abrir-s-d2054",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "301",
        "ok": "301",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "986",
        "ok": "986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "365",
        "ok": "365",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles1": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "percentiles2": {
        "total": "384",
        "ok": "384",
        "ko": "-"
    },
    "percentiles3": {
        "total": "511",
        "ok": "511",
        "ko": "-"
    },
    "percentiles4": {
        "total": "652",
        "ok": "652",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5992,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-1b83b": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
pathFormatted: "req_siette--abrir-s-1b83b",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - A Redirect 1",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "419",
        "ok": "419",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "809",
        "ok": "809",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles1": {
        "total": "725",
        "ok": "725",
        "ko": "-"
    },
    "percentiles2": {
        "total": "824",
        "ok": "824",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1267",
        "ok": "1267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1634",
        "ok": "1634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4345,
    "percentage": 72
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1252,
    "percentage": 21
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 403,
    "percentage": 7
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--confirm-0c992": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
pathFormatted: "req_siette--confirm-0c992",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - A",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2173",
        "ok": "2173",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "504",
        "ok": "504",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "110",
        "ok": "110",
        "ko": "-"
    },
    "percentiles1": {
        "total": "445",
        "ok": "445",
        "ko": "-"
    },
    "percentiles2": {
        "total": "516",
        "ok": "516",
        "ko": "-"
    },
    "percentiles3": {
        "total": "671",
        "ok": "671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "748",
        "ok": "748",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5951,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "460",
        "ok": "460",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "82",
        "ok": "82",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "percentiles3": {
        "total": "532",
        "ok": "532",
        "ko": "-"
    },
    "percentiles4": {
        "total": "716",
        "ok": "716",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5954,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 34,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 12,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "461",
        "ok": "461",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "percentiles3": {
        "total": "540",
        "ok": "540",
        "ko": "-"
    },
    "percentiles4": {
        "total": "720",
        "ok": "720",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5954,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 26,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "459",
        "ok": "459",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "84",
        "ok": "84",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "percentiles3": {
        "total": "515",
        "ok": "515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "710",
        "ok": "710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5954,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 28,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "79",
        "ok": "79",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "451",
        "ok": "451",
        "ko": "-"
    },
    "percentiles3": {
        "total": "511",
        "ok": "512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "691",
        "ok": "691",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5959,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "percentiles3": {
        "total": "508",
        "ok": "508",
        "ko": "-"
    },
    "percentiles4": {
        "total": "679",
        "ok": "679",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5970,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 11,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 19,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1547",
        "ok": "1547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "449",
        "ok": "449",
        "ko": "-"
    },
    "percentiles3": {
        "total": "498",
        "ok": "499",
        "ko": "-"
    },
    "percentiles4": {
        "total": "671",
        "ok": "671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5966,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 13,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2124",
        "ok": "2124",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "455",
        "ok": "455",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles1": {
        "total": "441",
        "ok": "441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "percentiles3": {
        "total": "505",
        "ok": "505",
        "ko": "-"
    },
    "percentiles4": {
        "total": "667",
        "ok": "667",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5973,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 10,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "459",
        "ok": "459",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "percentiles1": {
        "total": "443",
        "ok": "443",
        "ko": "-"
    },
    "percentiles2": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "percentiles3": {
        "total": "517",
        "ok": "517",
        "ko": "-"
    },
    "percentiles4": {
        "total": "676",
        "ok": "676",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5968,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 21,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 11,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "430",
        "ok": "430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles1": {
        "total": "442",
        "ok": "442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "513",
        "ok": "513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "699",
        "ok": "699",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5955,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 31,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 14,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "74",
        "ok": "74",
        "ko": "-"
    },
    "percentiles1": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "percentiles3": {
        "total": "498",
        "ok": "498",
        "ko": "-"
    },
    "percentiles4": {
        "total": "665",
        "ok": "665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5963,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 24,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 13,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "414",
        "ok": "414",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "percentiles1": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "444",
        "ok": "444",
        "ko": "-"
    },
    "percentiles3": {
        "total": "492",
        "ok": "492",
        "ko": "-"
    },
    "percentiles4": {
        "total": "577",
        "ok": "577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5975,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 15,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 10,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1927",
        "ok": "1927",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "133",
        "ok": "133",
        "ko": "-"
    },
    "percentiles1": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles2": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "percentiles3": {
        "total": "471",
        "ok": "471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "653",
        "ok": "653",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5964,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 16,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1916",
        "ok": "1916",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "119",
        "ok": "119",
        "ko": "-"
    },
    "percentiles1": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "percentiles4": {
        "total": "653",
        "ok": "653",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5963,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 19,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "440",
        "ok": "440",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2181",
        "ok": "2181",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "494",
        "ok": "494",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "113",
        "ok": "113",
        "ko": "-"
    },
    "percentiles1": {
        "total": "453",
        "ok": "453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "488",
        "ok": "488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "697",
        "ok": "697",
        "ko": "-"
    },
    "percentiles4": {
        "total": "912",
        "ok": "912",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5880,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 97,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 23,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1808",
        "ok": "1808",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "percentiles1": {
        "total": "235",
        "ok": "235",
        "ko": "-"
    },
    "percentiles2": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "percentiles3": {
        "total": "333",
        "ok": "333",
        "ko": "-"
    },
    "percentiles4": {
        "total": "425",
        "ok": "425",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5982,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "633",
        "ok": "633",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 6000,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "294",
        "ok": "294",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "972",
        "ok": "972",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "359",
        "ok": "359",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "percentiles1": {
        "total": "335",
        "ok": "335",
        "ko": "-"
    },
    "percentiles2": {
        "total": "378",
        "ok": "378",
        "ko": "-"
    },
    "percentiles3": {
        "total": "500",
        "ok": "500",
        "ko": "-"
    },
    "percentiles4": {
        "total": "634",
        "ok": "634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5994,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "6000",
        "ok": "6000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1523",
        "ok": "1523",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles1": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "percentiles4": {
        "total": "290",
        "ok": "290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5986,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "9.82",
        "ok": "9.82",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
