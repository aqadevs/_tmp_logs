var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "10",
        "ok": "9",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "520"
    },
    "maxResponseTime": {
        "total": "1027",
        "ok": "1027",
        "ko": "520"
    },
    "meanResponseTime": {
        "total": "343",
        "ok": "324",
        "ko": "520"
    },
    "standardDeviation": {
        "total": "247",
        "ok": "253",
        "ko": "0"
    },
    "percentiles1": {
        "total": "223",
        "ok": "221",
        "ko": "520"
    },
    "percentiles2": {
        "total": "326",
        "ok": "225",
        "ko": "520"
    },
    "percentiles3": {
        "total": "799",
        "ok": "760",
        "ko": "520"
    },
    "percentiles4": {
        "total": "981",
        "ok": "974",
        "ko": "520"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 80
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 10
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.5",
        "ok": "2.25",
        "ko": "0.25"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1027",
        "ok": "1027",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles3": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "224",
        "ok": "224",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles2": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles4": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "maxResponseTime": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "meanResponseTime": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "percentiles2": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "percentiles3": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "percentiles4": {
        "total": "520",
        "ok": "-",
        "ko": "520"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "-",
        "ko": "0.25"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.25",
        "ok": "0.25",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
