var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "214935",
        "ok": "214356",
        "ko": "579"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "214"
    },
    "maxResponseTime": {
        "total": "320213",
        "ok": "218899",
        "ko": "320213"
    },
    "meanResponseTime": {
        "total": "2518",
        "ok": "2499",
        "ko": "9514"
    },
    "standardDeviation": {
        "total": "3886",
        "ok": "3070",
        "ko": "45447"
    },
    "percentiles1": {
        "total": "1698",
        "ok": "1701",
        "ko": "600"
    },
    "percentiles2": {
        "total": "3263",
        "ok": "3267",
        "ko": "708"
    },
    "percentiles3": {
        "total": "7634",
        "ok": "7628",
        "ko": "21210"
    },
    "percentiles4": {
        "total": "13016",
        "ok": "12928",
        "ko": "320001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 59578,
    "percentage": 28
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23940,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 130838,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 579,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "121.022",
        "ok": "120.696",
        "ko": "0.326"
    }
},
contents: {
"req_siette--abrir-s-be296": {
        type: "REQUEST",
        name: "Siette> Abrir Siette",
path: "Siette> Abrir Siette",
pathFormatted: "req_siette--abrir-s-be296",
stats: {
    "name": "Siette> Abrir Siette",
    "numberOfRequests": {
        "total": "8000",
        "ok": "7993",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "842",
        "ok": "842",
        "ko": "15849"
    },
    "maxResponseTime": {
        "total": "129724",
        "ok": "21202",
        "ko": "129724"
    },
    "meanResponseTime": {
        "total": "1826",
        "ok": "1773",
        "ko": "63054"
    },
    "standardDeviation": {
        "total": "2596",
        "ok": "1323",
        "ko": "44213"
    },
    "percentiles1": {
        "total": "1376",
        "ok": "1376",
        "ko": "42355"
    },
    "percentiles2": {
        "total": "2087",
        "ok": "2086",
        "ko": "95267"
    },
    "percentiles3": {
        "total": "4063",
        "ok": "4057",
        "ko": "129598"
    },
    "percentiles4": {
        "total": "6634",
        "ok": "6536",
        "ko": "129699"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3342,
    "percentage": 42
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4651,
    "percentage": 58
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.505",
        "ok": "4.501",
        "ko": "0.004"
    }
}
    },"req_siette--abrir-s-ec7da": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 1",
path: "Siette> Abrir Siette Redirect 1",
pathFormatted: "req_siette--abrir-s-ec7da",
stats: {
    "name": "Siette> Abrir Siette Redirect 1",
    "numberOfRequests": {
        "total": "7993",
        "ok": "7993",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26185",
        "ok": "26185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "286",
        "ok": "286",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "416",
        "ok": "416",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "735",
        "ok": "735",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1297",
        "ok": "1297",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7814,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 10,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 169,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.501",
        "ok": "4.501",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-af03c": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 2",
path: "Siette> Abrir Siette Redirect 2",
pathFormatted: "req_siette--abrir-s-af03c",
stats: {
    "name": "Siette> Abrir Siette Redirect 2",
    "numberOfRequests": {
        "total": "7993",
        "ok": "7993",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4907",
        "ok": "4907",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "724",
        "ok": "724",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1243",
        "ok": "1243",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7883,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 92,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.501",
        "ok": "4.501",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-bf59e": {
        type: "REQUEST",
        name: "Siette> Abrir Siette Redirect 3",
path: "Siette> Abrir Siette Redirect 3",
pathFormatted: "req_siette--abrir-s-bf59e",
stats: {
    "name": "Siette> Abrir Siette Redirect 3",
    "numberOfRequests": {
        "total": "7993",
        "ok": "7993",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "85951",
        "ok": "85951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1769",
        "ok": "1769",
        "ko": "-"
    },
    "percentiles1": {
        "total": "896",
        "ok": "896",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1734",
        "ok": "1734",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5477",
        "ok": "5477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3280,
    "percentage": 41
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1480,
    "percentage": 19
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3233,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.501",
        "ok": "4.501",
        "ko": "-"
    }
}
    },"req_siette--login-a-00c34": {
        type: "REQUEST",
        name: "Siette> Login Action",
path: "Siette> Login Action",
pathFormatted: "req_siette--login-a-00c34",
stats: {
    "name": "Siette> Login Action",
    "numberOfRequests": {
        "total": "8000",
        "ok": "7999",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "300",
        "ok": "300",
        "ko": "17791"
    },
    "maxResponseTime": {
        "total": "102988",
        "ok": "102988",
        "ko": "17791"
    },
    "meanResponseTime": {
        "total": "1359",
        "ok": "1357",
        "ko": "17791"
    },
    "standardDeviation": {
        "total": "2509",
        "ok": "2502",
        "ko": "0"
    },
    "percentiles1": {
        "total": "751",
        "ok": "751",
        "ko": "17791"
    },
    "percentiles2": {
        "total": "1640",
        "ok": "1639",
        "ko": "17791"
    },
    "percentiles3": {
        "total": "4042",
        "ok": "4040",
        "ko": "17791"
    },
    "percentiles4": {
        "total": "7840",
        "ok": "7821",
        "ko": "17791"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4144,
    "percentage": 52
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1113,
    "percentage": 14
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2742,
    "percentage": 34
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.505",
        "ok": "4.504",
        "ko": "0.001"
    }
}
    },"req_siette--login-a-dbf55": {
        type: "REQUEST",
        name: "Siette> Login Action Redirect 1",
path: "Siette> Login Action Redirect 1",
pathFormatted: "req_siette--login-a-dbf55",
stats: {
    "name": "Siette> Login Action Redirect 1",
    "numberOfRequests": {
        "total": "7999",
        "ok": "7999",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "219",
        "ok": "219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "74131",
        "ok": "74131",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1346",
        "ok": "1346",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "percentiles1": {
        "total": "948",
        "ok": "948",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1783",
        "ok": "1783",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5679",
        "ok": "5679",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3201,
    "percentage": 40
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1473,
    "percentage": 18
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3325,
    "percentage": 42
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.504",
        "ok": "4.504",
        "ko": "-"
    }
}
    },"req_siette--mis-nex-a6db5": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST",
path: "Siette> Mis Next TEST",
pathFormatted: "req_siette--mis-nex-a6db5",
stats: {
    "name": "Siette> Mis Next TEST",
    "numberOfRequests": {
        "total": "8000",
        "ok": "7499",
        "ko": "501"
    },
    "minResponseTime": {
        "total": "528",
        "ok": "528",
        "ko": "528"
    },
    "maxResponseTime": {
        "total": "206906",
        "ok": "206906",
        "ko": "20839"
    },
    "meanResponseTime": {
        "total": "2382",
        "ok": "2495",
        "ko": "697"
    },
    "standardDeviation": {
        "total": "3675",
        "ok": "3761",
        "ko": "999"
    },
    "percentiles1": {
        "total": "1617",
        "ok": "1698",
        "ko": "586"
    },
    "percentiles2": {
        "total": "2942",
        "ok": "3084",
        "ko": "641"
    },
    "percentiles3": {
        "total": "6411",
        "ok": "6569",
        "ko": "864"
    },
    "percentiles4": {
        "total": "11184",
        "ok": "11321",
        "ko": "1600"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1587,
    "percentage": 20
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1037,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4875,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 501,
    "percentage": 6
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.505",
        "ok": "4.222",
        "ko": "0.282"
    }
}
    },"req_siette--logout-81b63": {
        type: "REQUEST",
        name: "Siette> logout",
path: "Siette> logout",
pathFormatted: "req_siette--logout-81b63",
stats: {
    "name": "Siette> logout",
    "numberOfRequests": {
        "total": "8000",
        "ok": "7997",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "16547"
    },
    "maxResponseTime": {
        "total": "320196",
        "ok": "37422",
        "ko": "320196"
    },
    "meanResponseTime": {
        "total": "1425",
        "ok": "1381",
        "ko": "118577"
    },
    "standardDeviation": {
        "total": "3986",
        "ok": "1766",
        "ko": "142570"
    },
    "percentiles1": {
        "total": "856",
        "ok": "856",
        "ko": "18987"
    },
    "percentiles2": {
        "total": "2024",
        "ok": "2024",
        "ko": "169592"
    },
    "percentiles3": {
        "total": "4141",
        "ok": "4122",
        "ko": "290075"
    },
    "percentiles4": {
        "total": "7340",
        "ok": "7261",
        "ko": "314172"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3202,
    "percentage": 40
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1397,
    "percentage": 17
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3398,
    "percentage": 42
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.505",
        "ok": "4.503",
        "ko": "0.002"
    }
}
    },"req_siette--logout--9e3a2": {
        type: "REQUEST",
        name: "Siette> logout Redirect 1",
path: "Siette> logout Redirect 1",
pathFormatted: "req_siette--logout--9e3a2",
stats: {
    "name": "Siette> logout Redirect 1",
    "numberOfRequests": {
        "total": "7997",
        "ok": "7997",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9079",
        "ok": "9079",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "415",
        "ok": "415",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "340",
        "ok": "340",
        "ko": "-"
    },
    "percentiles1": {
        "total": "321",
        "ok": "321",
        "ko": "-"
    },
    "percentiles2": {
        "total": "360",
        "ok": "360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "867",
        "ok": "867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1437",
        "ok": "1437",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 7111,
    "percentage": 89
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 683,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 203,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.503",
        "ok": "4.503",
        "ko": "-"
    }
}
    },"req_siette--logout--848ce": {
        type: "REQUEST",
        name: "Siette> logout Redirect 2",
path: "Siette> logout Redirect 2",
pathFormatted: "req_siette--logout--848ce",
stats: {
    "name": "Siette> logout Redirect 2",
    "numberOfRequests": {
        "total": "7996",
        "ok": "7996",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35618",
        "ok": "35618",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1314",
        "ok": "1314",
        "ko": "-"
    },
    "percentiles1": {
        "total": "649",
        "ok": "649",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1506",
        "ok": "1506",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5303",
        "ok": "5303",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4343,
    "percentage": 54
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 999,
    "percentage": 12
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2654,
    "percentage": 33
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.502",
        "ok": "4.502",
        "ko": "-"
    }
}
    },"req_siette--abrir-s-5e306": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B",
pathFormatted: "req_siette--abrir-s-5e306",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7497",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "303",
        "ok": "303",
        "ko": "17262"
    },
    "maxResponseTime": {
        "total": "59448",
        "ok": "59448",
        "ko": "17262"
    },
    "meanResponseTime": {
        "total": "1053",
        "ok": "1050",
        "ko": "17262"
    },
    "standardDeviation": {
        "total": "1855",
        "ok": "1846",
        "ko": "0"
    },
    "percentiles1": {
        "total": "363",
        "ok": "363",
        "ko": "17262"
    },
    "percentiles2": {
        "total": "1004",
        "ok": "1003",
        "ko": "17262"
    },
    "percentiles3": {
        "total": "3554",
        "ok": "3551",
        "ko": "17262"
    },
    "percentiles4": {
        "total": "7397",
        "ok": "7391",
        "ko": "17262"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5072,
    "percentage": 68
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 739,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1686,
    "percentage": 22
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_siette--abrir-s-3172f": {
        type: "REQUEST",
        name: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B Redirect 1",
path: "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B Redirect 1",
pathFormatted: "req_siette--abrir-s-3172f",
stats: {
    "name": "Siette> Abrir selección de test CONCURRENCIA - TEST DE PRUEBA - B Redirect 1",
    "numberOfRequests": {
        "total": "7497",
        "ok": "7497",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "639",
        "ok": "639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "67960",
        "ok": "67960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1914",
        "ok": "1914",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1519",
        "ok": "1519",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4645",
        "ok": "4645",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1458,
    "percentage": 19
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1557,
    "percentage": 21
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4482,
    "percentage": 60
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.221",
        "ok": "4.221",
        "ko": "-"
    }
}
    },"req_siette--confirm-5ae39": {
        type: "REQUEST",
        name: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - B",
path: "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - B",
pathFormatted: "req_siette--confirm-5ae39",
stats: {
    "name": "Siette> Confirmar selección de test CONCURRENCIA - TEST DE PRUEBA - B",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7497",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "433",
        "ok": "433",
        "ko": "60214"
    },
    "maxResponseTime": {
        "total": "73140",
        "ok": "73140",
        "ko": "60214"
    },
    "meanResponseTime": {
        "total": "4135",
        "ok": "4128",
        "ko": "60214"
    },
    "standardDeviation": {
        "total": "4093",
        "ok": "4042",
        "ko": "0"
    },
    "percentiles1": {
        "total": "3161",
        "ok": "3161",
        "ko": "60214"
    },
    "percentiles2": {
        "total": "5297",
        "ok": "5296",
        "ko": "60214"
    },
    "percentiles3": {
        "total": "10252",
        "ok": "10248",
        "ko": "60214"
    },
    "percentiles4": {
        "total": "17619",
        "ok": "17523",
        "ko": "60214"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 631,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 340,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6526,
    "percentage": 87
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p1-ec6ef": {
        type: "REQUEST",
        name: "p1",
path: "p1",
pathFormatted: "req_p1-ec6ef",
stats: {
    "name": "p1",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7495",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "20701"
    },
    "maxResponseTime": {
        "total": "320006",
        "ok": "60475",
        "ko": "320006"
    },
    "meanResponseTime": {
        "total": "3419",
        "ok": "3367",
        "ko": "133640"
    },
    "standardDeviation": {
        "total": "4811",
        "ok": "3051",
        "ko": "132764"
    },
    "percentiles1": {
        "total": "2530",
        "ok": "2527",
        "ko": "60212"
    },
    "percentiles2": {
        "total": "4343",
        "ok": "4340",
        "ko": "190109"
    },
    "percentiles3": {
        "total": "8682",
        "ok": "8656",
        "ko": "294027"
    },
    "percentiles4": {
        "total": "14865",
        "ok": "14694",
        "ko": "314810"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 658,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 722,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6115,
    "percentage": 82
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.22",
        "ko": "0.002"
    }
}
    },"req_p2-1d665": {
        type: "REQUEST",
        name: "p2",
path: "p2",
pathFormatted: "req_p2-1d665",
stats: {
    "name": "p2",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "370",
        "ok": "370",
        "ko": "20789"
    },
    "maxResponseTime": {
        "total": "80374",
        "ok": "80374",
        "ko": "41903"
    },
    "meanResponseTime": {
        "total": "3689",
        "ok": "3682",
        "ko": "31346"
    },
    "standardDeviation": {
        "total": "3494",
        "ok": "3461",
        "ko": "10557"
    },
    "percentiles1": {
        "total": "2750",
        "ok": "2749",
        "ko": "31346"
    },
    "percentiles2": {
        "total": "4727",
        "ok": "4725",
        "ko": "36625"
    },
    "percentiles3": {
        "total": "9502",
        "ok": "9485",
        "ko": "40847"
    },
    "percentiles4": {
        "total": "15413",
        "ok": "15087",
        "ko": "41692"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 559,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 555,
    "percentage": 7
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6382,
    "percentage": 85
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p3-7bc3c": {
        type: "REQUEST",
        name: "p3",
path: "p3",
pathFormatted: "req_p3-7bc3c",
stats: {
    "name": "p3",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7468",
        "ko": "30"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "215",
        "ko": "214"
    },
    "maxResponseTime": {
        "total": "218899",
        "ok": "218899",
        "ko": "9623"
    },
    "meanResponseTime": {
        "total": "3019",
        "ok": "3022",
        "ko": "2096"
    },
    "standardDeviation": {
        "total": "4219",
        "ok": "4225",
        "ko": "2156"
    },
    "percentiles1": {
        "total": "2203",
        "ok": "2207",
        "ko": "1341"
    },
    "percentiles2": {
        "total": "3667",
        "ok": "3671",
        "ko": "1935"
    },
    "percentiles3": {
        "total": "7656",
        "ok": "7649",
        "ko": "7255"
    },
    "percentiles4": {
        "total": "13172",
        "ok": "13182",
        "ko": "9212"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 665,
    "percentage": 9
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 963,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5840,
    "percentage": 78
},
    "group4": {
    "name": "failed",
    "count": 30,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.205",
        "ko": "0.017"
    }
}
    },"req_p4-13207": {
        type: "REQUEST",
        name: "p4",
path: "p4",
pathFormatted: "req_p4-13207",
stats: {
    "name": "p4",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7495",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "15850"
    },
    "maxResponseTime": {
        "total": "130848",
        "ok": "76280",
        "ko": "130848"
    },
    "meanResponseTime": {
        "total": "4222",
        "ok": "4202",
        "ko": "54183"
    },
    "standardDeviation": {
        "total": "3982",
        "ok": "3699",
        "ko": "54210"
    },
    "percentiles1": {
        "total": "3211",
        "ok": "3209",
        "ko": "15852"
    },
    "percentiles2": {
        "total": "5664",
        "ok": "5658",
        "ko": "73350"
    },
    "percentiles3": {
        "total": "10374",
        "ok": "10347",
        "ko": "119348"
    },
    "percentiles4": {
        "total": "16169",
        "ok": "16151",
        "ko": "128548"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 552,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 325,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6618,
    "percentage": 88
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.22",
        "ko": "0.002"
    }
}
    },"req_p5-ed92e": {
        type: "REQUEST",
        name: "p5",
path: "p5",
pathFormatted: "req_p5-ed92e",
stats: {
    "name": "p5",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "129787"
    },
    "maxResponseTime": {
        "total": "320001",
        "ok": "59625",
        "ko": "320001"
    },
    "meanResponseTime": {
        "total": "3528",
        "ok": "3469",
        "ko": "224894"
    },
    "standardDeviation": {
        "total": "5113",
        "ok": "3264",
        "ko": "95107"
    },
    "percentiles1": {
        "total": "2589",
        "ok": "2589",
        "ko": "224894"
    },
    "percentiles2": {
        "total": "4435",
        "ok": "4434",
        "ko": "272448"
    },
    "percentiles3": {
        "total": "9303",
        "ok": "9292",
        "ko": "310490"
    },
    "percentiles4": {
        "total": "15321",
        "ok": "15307",
        "ko": "318099"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 634,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 692,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6170,
    "percentage": 82
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p6-c6c27": {
        type: "REQUEST",
        name: "p6",
path: "p6",
pathFormatted: "req_p6-c6c27",
stats: {
    "name": "p6",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "17086"
    },
    "maxResponseTime": {
        "total": "75519",
        "ok": "75519",
        "ko": "18008"
    },
    "meanResponseTime": {
        "total": "3298",
        "ok": "3295",
        "ko": "17547"
    },
    "standardDeviation": {
        "total": "3090",
        "ok": "3081",
        "ko": "461"
    },
    "percentiles1": {
        "total": "2434",
        "ok": "2433",
        "ko": "17547"
    },
    "percentiles2": {
        "total": "4223",
        "ok": "4222",
        "ko": "17778"
    },
    "percentiles3": {
        "total": "8725",
        "ok": "8703",
        "ko": "17962"
    },
    "percentiles4": {
        "total": "13873",
        "ok": "13780",
        "ko": "17999"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 597,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 778,
    "percentage": 10
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6121,
    "percentage": 82
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p7-46d46": {
        type: "REQUEST",
        name: "p7",
path: "p7",
pathFormatted: "req_p7-46d46",
stats: {
    "name": "p7",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7494",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "18414"
    },
    "maxResponseTime": {
        "total": "320000",
        "ok": "72423",
        "ko": "320000"
    },
    "meanResponseTime": {
        "total": "3477",
        "ok": "3426",
        "ko": "99599"
    },
    "standardDeviation": {
        "total": "4864",
        "ok": "3171",
        "ko": "127503"
    },
    "percentiles1": {
        "total": "2595",
        "ok": "2594",
        "ko": "29992"
    },
    "percentiles2": {
        "total": "4406",
        "ok": "4397",
        "ko": "109413"
    },
    "percentiles3": {
        "total": "8894",
        "ok": "8864",
        "ko": "277883"
    },
    "percentiles4": {
        "total": "14869",
        "ok": "14669",
        "ko": "311577"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 562,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 624,
    "percentage": 8
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6308,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.22",
        "ko": "0.002"
    }
}
    },"req_p8-2e3f2": {
        type: "REQUEST",
        name: "p8",
path: "p8",
pathFormatted: "req_p8-2e3f2",
stats: {
    "name": "p8",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "223",
        "ok": "223",
        "ko": "21163"
    },
    "maxResponseTime": {
        "total": "320212",
        "ok": "75368",
        "ko": "320212"
    },
    "meanResponseTime": {
        "total": "3474",
        "ok": "3430",
        "ko": "170688"
    },
    "standardDeviation": {
        "total": "4812",
        "ok": "3120",
        "ko": "149525"
    },
    "percentiles1": {
        "total": "2606",
        "ok": "2605",
        "ko": "170688"
    },
    "percentiles2": {
        "total": "4436",
        "ok": "4436",
        "ko": "245450"
    },
    "percentiles3": {
        "total": "8852",
        "ok": "8841",
        "ko": "305260"
    },
    "percentiles4": {
        "total": "14711",
        "ok": "14611",
        "ko": "317222"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 547,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 648,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6301,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p9-d9a9d": {
        type: "REQUEST",
        name: "p9",
path: "p9",
pathFormatted: "req_p9-d9a9d",
stats: {
    "name": "p9",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "17693"
    },
    "maxResponseTime": {
        "total": "320064",
        "ok": "61653",
        "ko": "320064"
    },
    "meanResponseTime": {
        "total": "3489",
        "ok": "3444",
        "ko": "168879"
    },
    "standardDeviation": {
        "total": "4910",
        "ok": "3273",
        "ko": "151186"
    },
    "percentiles1": {
        "total": "2602",
        "ok": "2602",
        "ko": "168879"
    },
    "percentiles2": {
        "total": "4327",
        "ok": "4324",
        "ko": "244471"
    },
    "percentiles3": {
        "total": "8910",
        "ok": "8892",
        "ko": "304945"
    },
    "percentiles4": {
        "total": "14759",
        "ok": "14600",
        "ko": "317040"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 558,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 644,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6294,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p10-ad304": {
        type: "REQUEST",
        name: "p10",
path: "p10",
pathFormatted: "req_p10-ad304",
stats: {
    "name": "p10",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "131018"
    },
    "maxResponseTime": {
        "total": "320213",
        "ok": "72515",
        "ko": "320213"
    },
    "meanResponseTime": {
        "total": "3474",
        "ok": "3415",
        "ko": "225616"
    },
    "standardDeviation": {
        "total": "5129",
        "ok": "3279",
        "ko": "94598"
    },
    "percentiles1": {
        "total": "2576",
        "ok": "2576",
        "ko": "225616"
    },
    "percentiles2": {
        "total": "4367",
        "ok": "4363",
        "ko": "272914"
    },
    "percentiles3": {
        "total": "8773",
        "ok": "8760",
        "ko": "310753"
    },
    "percentiles4": {
        "total": "15142",
        "ok": "15053",
        "ko": "318321"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 552,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 662,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6282,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p11-52053": {
        type: "REQUEST",
        name: "p11",
path: "p11",
pathFormatted: "req_p11-52053",
stats: {
    "name": "p11",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "60215"
    },
    "maxResponseTime": {
        "total": "320001",
        "ok": "60890",
        "ko": "320001"
    },
    "meanResponseTime": {
        "total": "3376",
        "ok": "3326",
        "ko": "190108"
    },
    "standardDeviation": {
        "total": "4791",
        "ok": "3026",
        "ko": "129893"
    },
    "percentiles1": {
        "total": "2552",
        "ok": "2552",
        "ko": "190108"
    },
    "percentiles2": {
        "total": "4188",
        "ok": "4185",
        "ko": "255055"
    },
    "percentiles3": {
        "total": "8713",
        "ok": "8704",
        "ko": "307012"
    },
    "percentiles4": {
        "total": "13661",
        "ok": "13625",
        "ko": "317403"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 559,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 656,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6281,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p12-d3d07": {
        type: "REQUEST",
        name: "p12",
path: "p12",
pathFormatted: "req_p12-d3d07",
stats: {
    "name": "p12",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7495",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "42756"
    },
    "maxResponseTime": {
        "total": "320000",
        "ok": "74815",
        "ko": "320000"
    },
    "meanResponseTime": {
        "total": "3393",
        "ok": "3338",
        "ko": "141400"
    },
    "standardDeviation": {
        "total": "4912",
        "ok": "3179",
        "ko": "126519"
    },
    "percentiles1": {
        "total": "2529",
        "ok": "2528",
        "ko": "61445"
    },
    "percentiles2": {
        "total": "4215",
        "ok": "4212",
        "ko": "190723"
    },
    "percentiles3": {
        "total": "8753",
        "ok": "8721",
        "ko": "294145"
    },
    "percentiles4": {
        "total": "14485",
        "ok": "14232",
        "ko": "314829"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 562,
    "percentage": 7
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 694,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6239,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.22",
        "ko": "0.002"
    }
}
    },"req_p13-f4fc7": {
        type: "REQUEST",
        name: "p13",
path: "p13",
pathFormatted: "req_p13-f4fc7",
stats: {
    "name": "p13",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "220",
        "ok": "220",
        "ko": "26648"
    },
    "maxResponseTime": {
        "total": "320000",
        "ok": "74361",
        "ko": "320000"
    },
    "meanResponseTime": {
        "total": "3367",
        "ok": "3322",
        "ko": "173324"
    },
    "standardDeviation": {
        "total": "4798",
        "ok": "3094",
        "ko": "146676"
    },
    "percentiles1": {
        "total": "2547",
        "ok": "2546",
        "ko": "173324"
    },
    "percentiles2": {
        "total": "4254",
        "ok": "4251",
        "ko": "246662"
    },
    "percentiles3": {
        "total": "8698",
        "ok": "8685",
        "ko": "305332"
    },
    "percentiles4": {
        "total": "13255",
        "ok": "13174",
        "ko": "317066"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 579,
    "percentage": 8
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 667,
    "percentage": 9
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6250,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_p14-989ae": {
        type: "REQUEST",
        name: "p14",
path: "p14",
pathFormatted: "req_p14-989ae",
stats: {
    "name": "p14",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7495",
        "ko": "3"
    },
    "minResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "16151"
    },
    "maxResponseTime": {
        "total": "320000",
        "ok": "75525",
        "ko": "320000"
    },
    "meanResponseTime": {
        "total": "4964",
        "ok": "4919",
        "ko": "117823"
    },
    "standardDeviation": {
        "total": "5530",
        "ok": "4161",
        "ko": "142961"
    },
    "percentiles1": {
        "total": "3936",
        "ok": "3934",
        "ko": "17319"
    },
    "percentiles2": {
        "total": "6354",
        "ok": "6351",
        "ko": "168660"
    },
    "percentiles3": {
        "total": "11990",
        "ok": "11971",
        "ko": "289732"
    },
    "percentiles4": {
        "total": "19218",
        "ok": "19166",
        "ko": "313946"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 441,
    "percentage": 6
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 161,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 6893,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 3,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.22",
        "ko": "0.002"
    }
}
    },"req_pregunta--send--45f05": {
        type: "REQUEST",
        name: "pregunta, send empty 15",
path: "pregunta, send empty 15",
pathFormatted: "req_pregunta--send--45f05",
stats: {
    "name": "pregunta, send empty 15",
    "numberOfRequests": {
        "total": "7498",
        "ok": "7496",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "226",
        "ok": "226",
        "ko": "15849"
    },
    "maxResponseTime": {
        "total": "45142",
        "ok": "45142",
        "ko": "41611"
    },
    "meanResponseTime": {
        "total": "2391",
        "ok": "2384",
        "ko": "28730"
    },
    "standardDeviation": {
        "total": "2399",
        "ok": "2351",
        "ko": "12881"
    },
    "percentiles1": {
        "total": "1692",
        "ok": "1690",
        "ko": "28730"
    },
    "percentiles2": {
        "total": "3232",
        "ok": "3230",
        "ko": "35171"
    },
    "percentiles3": {
        "total": "6467",
        "ok": "6455",
        "ko": "40323"
    },
    "percentiles4": {
        "total": "10269",
        "ok": "10115",
        "ko": "41353"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1827,
    "percentage": 24
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 961,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4708,
    "percentage": 63
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.222",
        "ok": "4.221",
        "ko": "0.001"
    }
}
    },"req_siette--mis-nex-9325f": {
        type: "REQUEST",
        name: "Siette> Mis Next TEST Redirect 1",
path: "Siette> Mis Next TEST Redirect 1",
pathFormatted: "req_siette--mis-nex-9325f",
stats: {
    "name": "Siette> Mis Next TEST Redirect 1",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "maxResponseTime": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "meanResponseTime": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "percentiles2": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "percentiles3": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "percentiles4": {
        "total": "2244",
        "ok": "-",
        "ko": "2244"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.001",
        "ok": "-",
        "ko": "0.001"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
